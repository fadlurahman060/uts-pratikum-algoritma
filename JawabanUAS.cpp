//Nama : Fadlurrahman Habibullah Ramadhana
//NIM : 301230027

#include <iostream>
using namespace std;

void tampil_nilai(double absen, double tugas, double quiz, double uts, double uas){
	cout << "Absen : " << absen << endl;
	cout << "Tugas : " << tugas << endl;
	cout << "Quiz : " << quiz << endl;
	cout << "UTS : " << uts << endl;
	cout << "UAS : " << uas << endl;
}

char huruf_mutu(double nilai){
	char hurufMutu;
	if (nilai > 85 && nilai <= 100)
		hurufMutu = 'A';
	else if (nilai > 70 && nilai <= 85)
		hurufMutu = 'B';
	else if (nilai > 55 && nilai <= 70)
		hurufMutu = 'C';
	else if (nilai > 40 && nilai <= 55)
		hurufMutu = 'D';
	else if (nilai >= 0 && nilai <= 40)
		hurufMutu = 'E';
	return hurufMutu;

}

double hitung_nilai(double absen, double tugas, double quiz, double uts, double uas){
	return ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
}

int main(){
	double absen, tugas, quiz, UTS, UAS, nilai;
	char Huruf_Mutu;

	absen = 100;
	tugas = 80;
	quiz = 40;
	UTS = 60;
	UAS = 50;

	tampil_nilai(absen, tugas, quiz, UTS, UAS);

	nilai = hitung_nilai(absen, tugas, quiz, UTS, UAS);
	Huruf_Mutu = huruf_mutu(nilai);

	cout << "Huruf Mutu : " << Huruf_Mutu << endl;

	return 0;
}
